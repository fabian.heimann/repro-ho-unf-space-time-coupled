# Reproduction material for "A Higher Order Unfitted Space-Time Finite Element Method for Coupled Surface-Bulk problems" by F. Heimann

This repository contains the scripts necessary to reproduce the data of the numerical experiments of the paper.

The computations are based on the software ngsxfem, where we used the commit f0bdfb897514746efa4e4ef6ffc250468d106e51.

## Setup ngsxfem to run the examples.
Our necessary software can be supplied in several forms. Check out the [`github page`](http://github.com/ngsxfem/ngsxfem), where several installation routines, including compilation from sourcce, are explained.

## Reproduction scripts
The commands to perform the numerical studies of the paper are collected in the bash files `runs.sh` and `runsNL.sh`. The python scripts come with a straightforward bash command line option parser. (C.f. also the sh-files for example calls.)
