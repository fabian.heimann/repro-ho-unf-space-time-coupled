#!/bin/bash
#SBATCH -p medium
#SBATCH -t 12:00:00
#SBATCH -N 1
#SBATCH -n 8

source ~/.bashrc

/usr/bin/time -v python3 spacetimeDG_unfitted_lin_coupled.py --ks 1 --kt 1 --ktls 1 --from 0 --to 7
/usr/bin/time -v python3 spacetimeDG_unfitted_lin_coupled.py --ks 2 --kt 2 --ktls 2 --from 0 --to 6
/usr/bin/time -v python3 spacetimeDG_unfitted_lin_coupled.py --ks 3 --kt 3 --ktls 3 --from 0 --to 5
/usr/bin/time -v python3 spacetimeDG_unfitted_lin_coupled.py --ks 4 --kt 4 --ktls 4 --from 0 --to 4
