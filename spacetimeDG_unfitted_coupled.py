# ------------------------------ LOAD LIBRARIES -------------------------------
from ngsolve import *
from netgen.geom2d import SplineGeometry
from xfem import *
from math import pi
from xfem.lset_spacetime import *
ngsglobals.msg_level = 1
from time import time

def solve_coupled_ST(i=1, ks=2, kt=2, ktls = 2, gamma=0.05, nthr=6):
    SetNumThreads(nthr)

    # -------------------------------- PARAMETERS ---------------------------------
    # DISCRETIZATION PARAMETERS:

    # Parameter for refinement study:
    n_steps = 2**(i+1)
    space_refs = i

    # Polynomial order in time
    k_t = kt
    # Poloymial order in space
    k_s = ks
    # Polynomial order in time for level set approximation
    lset_order_time = ktls
    # Integration order in time
    time_order = 4 * k_t + 2
    # Time stepping parameters
    tstart = 0
    tend = 0.5
    delta_t = (tend - tstart) / n_steps
    maxh = 0.2
    # Ghost-penalty parameter
    gamma = gamma
    # Map from reference time to physical time
    told = Parameter(tstart)
    t = told + delta_t * tref

    # PROBLEM SETUP:
    k_B = 0.01
    k_S = 1
    bB = 1
    bS = 1
    bBS = 1

    # Outer domain:
    rect = SplineGeometry()
    rect.AddRectangle([0, 0], [1, 1])

    # Level set geometry
    # Radius of disk (the geometry)
    R = 0.18
    # Position shift of the geometry in time
    xc = 0.5 + 0.28 * sin(pi * t)
    yc = 0.5 - 0.28 * cos(pi * t)
    # Convection velocity:
    w = CoefficientFunction(( pi*(0.5-y), pi*(x-0.5)))
    # Level set
    r = sqrt((x - xc)**2 + (y - yc)**2)
    levelset = R - r

    # Solution
    u_exact_B = 0.5 + 0.4*cos(pi*x)*cos(pi*y)*cos(2*pi*t)
    coeff_f_B = (u_exact_B.Diff(t)
            - k_B * (u_exact_B.Diff(x).Diff(x) + u_exact_B.Diff(y).Diff(y))
            + w[0] * u_exact_B.Diff(x) + w[1] * u_exact_B.Diff(y)).Compile()

    Dims = [x,y]
    grad_symb = lambda f: CoefficientFunction( tuple( [f.Diff(d) for d in Dims]) )
    normal = grad_symb(levelset)/ Norm(grad_symb(levelset))

    f_coupling = -k_B * InnerProduct(grad_symb(u_exact_B), normal)

    u_exact_S = (bB * u_exact_B - f_coupling)/(bS + bBS * u_exact_B)

    Lapl = sum([u_exact_S.Diff(d).Diff(d) for d in Dims])
    hesse = CoefficientFunction ( tuple( [u_exact_S.Diff(d1).Diff(d2) for d1 in Dims for d2 in Dims]), dims=(len(Dims),len(Dims)))

    surface_Lapl_u = Lapl - InnerProduct(normal, hesse*normal) - sum([normal[di].Diff(Dims[di]) for di in range(len(Dims))])*InnerProduct(grad_symb(u_exact_S), normal)
    surface_div_w = sum([ w[di].Diff(Dims[di]) for di in range(len(Dims)) ]) + sum([ normal[di]*normal[dj]*w[di].Diff(Dims[dj]) for di in range(len(Dims)) for dj in range(len(Dims)) ])

    # R.h.s.
    coeff_f_S = (u_exact_S.Diff(t) - k_S*surface_Lapl_u + surface_div_w*u_exact_S + InnerProduct(grad_symb(u_exact_S), w) - f_coupling).Compile()

    # ----------------------------------- MAIN ------------------------------------
    ngmesh = rect.GenerateMesh(maxh=maxh, quad_dominated=False)
    for j in range(space_refs):
        ngmesh.Refine()
    mesh = Mesh(ngmesh)

    # Spatial FESpace for solution
    fes1 = H1(mesh, order=k_s, dgjumps=True)
    # Time finite element (nodal!)
    tfe = ScalarTimeFE(k_t)
    # (Tensor product) space-time finite element space
    st_fes = tfe * fes1

    st_coup = st_fes * st_fes

    # # Space time version of Levelset Mesh Adapation object. Also offers integrator
    # # helper functions that involve the correct mesh deformation
    lsetadap = LevelSetMeshAdaptation_Spacetime(mesh, order_space=k_s,
                                                order_time=lset_order_time,
                                                threshold=0.1,
                                                discontinuous_qn=True)

    gfu = GridFunction(st_coup)
    w_N = GridFunction(st_coup)

    u_B_last = GridFunction(fes1)
    u_S_last = GridFunction(fes1)

    fes_l2 = L2(mesh, order=k_s, dgjumps=True)
    u_B_last_l2 = GridFunction(fes_l2)
    u_S_last_l2 = GridFunction(fes_l2)

    (w_B,w_S), (v_B,v_S) = st_coup.TnT()
    h = specialcf.mesh_size

    ba_facets = BitArray(mesh.nfacet)
    active_dofs = BitArray (st_coup.ndof)
    ci = CutInfo(mesh, time_order=0)

    dGst = delta_t * dCut(lsetadap.levelsetp1[INTERVAL], IF, time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(IF))
    dGold = dCut(lsetadap.levelsetp1[BOTTOM], IF,
                deformation=lsetadap.deformation[BOTTOM],
                definedonelements=ci.GetElementsOfType(IF), tref=0)
    dGnew = dCut(lsetadap.levelsetp1[TOP], IF,
                deformation=lsetadap.deformation[TOP],
                definedonelements=ci.GetElementsOfType(IF), tref=1)
    dGstab = dxtref(mesh,time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(IF))

    dQ = delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(HASNEG))
    dOmold = dCut(lsetadap.levelsetp1[BOTTOM], NEG,
                deformation=lsetadap.deformation[BOTTOM],
                definedonelements=ci.GetElementsOfType(HASNEG), tref=0)
    dOmnew = dCut(lsetadap.levelsetp1[TOP], NEG,
                deformation=lsetadap.deformation[TOP],
                definedonelements=ci.GetElementsOfType(HASNEG), tref=1)
    dw = delta_t * dFacetPatch(definedonelements=ba_facets, time_order=time_order,
                            deformation=lsetadap.deformation[INTERVAL])

    def dt(u):
        return 1.0 / delta_t * dtref(u)

    def P(u,n_phi):
        return u - (u*n_phi)*n_phi

    #normalization (pointwise) of a vector
    def Normalized(u):
        return 1.0 / Norm(u) * u

    n_phi1 = Normalized(grad(lsetadap.lset_p1))

    discr_surface_div_w = sum([ w[di].Diff(Dims[di]) for di in range(len(Dims)) ]) + sum([ n_phi1[di]*n_phi1[dj]*w[di].Diff(Dims[dj]) for di in range(len(Dims)) for dj in range(len(Dims)) ])

    u_B = gfu.components[0]
    u_S = gfu.components[1]

    DF = RestrictedBilinearForm(st_coup, "a", check_unused=False, symmetric=False)
    stab = RestrictedBilinearForm(st_coup, "J", check_unused=False, symmetric=False)
    F = LinearForm(st_coup)

    F += bB * v_B * (dt(u_B) - dt(lsetadap.deform) * grad(u_B)) * dQ
    F += bS * v_S * (dt(u_S) - dt(lsetadap.deform) * grad(u_S)) * dGst

    F += bS*(k_S*InnerProduct(P( grad(u_S), n_phi1), P( grad(v_S), n_phi1)) + v_S * InnerProduct(w, grad(u_S)) + v_S * u_S* discr_surface_div_w) * dGst
    F += (bB*u_B - bS*u_S)*(bB*v_B - bS*v_S)* dGst
    F += bB* (k_B*InnerProduct( grad(u_B), grad(v_B)) + v_B * InnerProduct(w, grad(u_B)) ) * dQ

    F += bBS * u_B * u_S *(bS *v_S - bB *v_B)*dGst

    F += bB*(u_B - u_B_last_l2) * v_B * dOmold
    F += bS* (u_S - u_S_last_l2) * v_S * dGold
    F += gamma* (n_phi1 * grad(u_S)) * (n_phi1 * grad(v_S))* dGstab

    F += -bB*coeff_f_B * v_B * dQ
    F += -bS*coeff_f_S * v_S * dGst

    DF += bB * v_B * (dt(w_B) - dt(lsetadap.deform) * grad(w_B)) * dQ
    DF += bS * v_S * (dt(w_S) - dt(lsetadap.deform) * grad(w_S)) * dGst
    DF += bS*(k_S*InnerProduct(P( grad(w_S), n_phi1), P( grad(v_S), n_phi1)) + v_S * InnerProduct(w, grad(w_S)) + v_S * w_S* discr_surface_div_w) * dGst
    DF += (bB*w_B - bS*w_S)*(bB*v_B - bS*v_S)* dGst
    DF += bB* (k_B*InnerProduct( grad(w_B), grad(v_B)) + v_B * InnerProduct(w, grad(w_B)) ) * dQ

    DF += (bBS * w_B * u_S + bBS * u_B * w_S)*(bS *v_S - bB *v_B)*dGst

    DF += bB * w_B * v_B * dOmold
    DF += bS* w_S* v_S * dGold
    DF += gamma* (n_phi1 * grad(w_S)) * (n_phi1 * grad(v_S))* dGstab

    stab += h**(-2) * (1 + delta_t / h) * gamma * (w_B - w_B.Other()) * (v_B - v_B.Other()) * dw

    # Set initial values
    u_B_last_l2.Set(fix_tref(u_exact_B, 0))
    u_S_last_l2.Set(fix_tref(u_exact_S, 0))
    # Project u_last at the beginning of each time step
    lsetadap.ProjectOnUpdate(u_B_last_l2)
    lsetadap.ProjectOnUpdate(u_S_last_l2)

    NewtonStat = []

    while tend - told.Get() > delta_t / 2:
        with TaskManager():
            lsetadap.CalcDeformation(levelset)

        # Update markers in (space-time) mesh
        ci.Update(lsetadap.levelsetp1[INTERVAL], time_order=0)

        ba_facets[:] = GetFacetsWithNeighborTypes(mesh, a=ci.GetElementsOfType(HASNEG), b=ci.GetElementsOfType(IF))
        active_dofs[0:st_fes.ndof] =  GetDofsOfElements(st_fes, ci.GetElementsOfType(HASNEG))
        active_dofs[st_fes.ndof:] = GetDofsOfElements(st_fes, ci.GetElementsOfType(IF))

        gfu.components[0].Set(u_B_last_l2)
        gfu.components[1].Set(u_S_last_l2)

        with TaskManager():
            stab.Assemble(reallocate=True)

        for iNew in range(60):
            with TaskManager():
                F.Assemble()
                F.vec.data += stab.mat * gfu.vec

                DF.Assemble(reallocate=True)
                DF.mat.AsVector().data = DF.mat.AsVector() + stab.mat.AsVector()

                w_N.vec.data = DF.mat.Inverse(active_dofs, inverse="umfpack") * F.vec
                print("Norm w_N (no. ", iNew, "): ", Norm(w_N.vec))
                # Solve linear system
                gfu.vec.data -= w_N.vec
                if Norm(w_N.vec) < 1e-9:
                    break
                if Norm(w_N.vec) > 1e5:
                     iNew = 59
                     gfu.components[0].Set(u_B_last_l2)
                     gfu.components[1].Set(u_S_last_l2)
                     break
        if iNew == 59:
            for iNew in range(60,100):
                with TaskManager():
                    # print("Will Assemble F")
                    F.Assemble()
                    F.vec.data += stab.mat * gfu.vec
        
                    # print("Will Assemble DF")
                    DF.Assemble()
                    DF.mat.AsVector().data = DF.mat.AsVector() + stab.mat.AsVector()
                    # RestrictGFInTime(spacetime_gf=gfu.components[0], reference_time=1.0, space_gf=u_B_last)
                    # Draw(u_B_last, mesh, "u_B_last")
                    # input()
        
                    w_N.vec.data = DF.mat.Inverse(active_dofs, inverse="umfpack") * F.vec
                    print("Norm w_N (no. ", iNew, "): ", Norm(w_N.vec))
                    # Solve linear system
                    gfu.vec.data -= 0.8*w_N.vec
                    if Norm(w_N.vec) < 2e-10:
                        break
        NewtonStat.append(iNew)

        # Evaluate upper trace of solution for
        #  * for error evaluation
        #  * upwind-coupling to next time slab
        RestrictGFInTime(spacetime_gf=gfu.components[0], reference_time=1.0, space_gf=u_B_last)
        RestrictGFInTime(spacetime_gf=gfu.components[1], reference_time=1.0, space_gf=u_S_last)
        #RestrictGFInTime(spacetime_gf=gfu, reference_time=1.0, space_gf=u_last)

        u_B_last_l2.Set(u_B_last)
        u_S_last_l2.Set(u_S_last)

        # Compute error at final time
        l2error = sqrt(Integrate((u_exact_B - u_B_last)**2 * dOmnew, mesh))
        l2errorS = sqrt(Integrate((u_exact_S - u_S_last)**2 * dGnew, mesh))

        # Draw(fix_tref(levelset,0), mesh, "levelset_0")
        # Draw(fix_tref(levelset,1), mesh, "levelset_1")

        Draw(fix_tref(u_exact_B,1), mesh, "u_exact_B")
        Draw(u_B_last, mesh, "u_B_last")
        # input()

        # Update time variable (ParameterCL)
        told.Set(told.Get() + delta_t)
        print("\rt = {0:12.9f}, L2 error = {1:12.9e}".format(told.Get(), l2error))
        print("\rt = {0:12.9f}, L2 error (S) = {1:12.9e}".format(told.Get(), l2errorS))
        # break

    print("NewtonStat: Max: ", max(NewtonStat))
    print("NewtonStat: Avg: ", sum(NewtonStat)/len(NewtonStat))

    return (l2error, l2errorS, max(NewtonStat), sum(NewtonStat)/len(NewtonStat))

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Solve convection-diffusion problem on a coupled mixed-dim moving domain by a spacetime method.')
    parser.add_argument('-ks','--ks', type=int, default=3, help='order of space discretisation & isoparam mapping, default: 3')
    parser.add_argument('-kt','--kt', type=int, default=3, help='order of time discretisation, default: 3. For GCC, 3 will be used independent of this parameter.')
    parser.add_argument('-ktls','--ktls', type=int, default=3, help='order of time discretisation for lset deformation, default: 3.')
    parser.add_argument('-to','--to', type=int, default=3, help='finest level of st-refinement, default: 3')
    parser.add_argument('-from','--from', type=int, default=0, help='coarsest level of st-refinement, default: 0')
    parser.add_argument('-gamma','--gamma', type=float, default=0.05, help='Ghost Penalty stabilisation constant, default: 0.05')
    # parser.add_argument('-sm','--struct_mesh', type=int, default=0, help='Use a structured mesh, enter either int 0 for false or int 1 for true. 2 for reproduction unstruct meshes. default: 0')
    # parser.add_argument('-ro','--ref_offset', type=int, default=0, help='offset of the used level for the non-refined discretisation (space/ time). Only relevant for rs not both. default: 0')
    parser.add_argument('-nthr','--nthr', type=int, default=6, help='Number of threads for parallel assembly. default: 6')
    # parser.add_argument('-sol','--sol', type=str, default="umfpack", help='Direct solver to be used. default: umfpack')
    # parser.add_argument('-cmd','--cmd', type=int, default=0, help='Calc Max Distance. Activate with 1, deactivate with 0. Only available with DG. default: 0')
    # parser.add_argument('-nti', '--nti', type=int, default=0, help='Use Naive time-integration for benchmarking purposes')

    args = parser.parse_args()
    options = vars(args)

    bulk_errs = []
    surf_errs = []

    outfile = "out/conv_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+"_from"+str(options["from"])+"_to"+str(options["to"])+".dat"
    f = open(outfile, "w")
    f.close()

    for i in range(options["from"], options["to"]+1):
        a = time()
        (l2B, l2S, N1, N2) = solve_coupled_ST(i, options["ks"], options["kt"], options["ktls"], options["gamma"], options["nthr"])
        b = time()
        bulk_errs.append(l2B)
        surf_errs.append(l2S)
        f = open(outfile, "a")
        f.write(str(i)+"\t"+str(l2B)+"\t"+str(l2S)+"\t"+str(N1)+"\t"+str(N2)+"\t"+str(b-a)+"\n")
        f.close()

    print("final l2 error stat B: ", bulk_errs)
    print("final l2 error stat S: ", surf_errs)

    eocsB = [log(bulk_errs[j-1]/bulk_errs[j])/log(2) for j in range(1,len(bulk_errs))]
    eocsS = [log(surf_errs[j-1]/surf_errs[j])/log(2) for j in range(1,len(surf_errs))]

    print ("eocs (l2final B) : ", eocsB)
    print ("eocs (l2final S) : ", eocsS)
